package com.goodbackend.service.worker;

import com.goodbackend.service.pgnotify.PgNotifyService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.concurrent.atomic.AtomicInteger;

@SpringBootTest
public class TestWorker {

	private final WorkerProcessor myWorkerProcessor;
	private final PgNotifyService pgNotifyService;

	@Autowired
	public TestWorker(WorkerProcessor myWorkerProcessor, PgNotifyService pgNotifyService) {
		this.myWorkerProcessor = myWorkerProcessor;
		this.pgNotifyService = pgNotifyService;
	}

	@Test
	public void testWorker() {
		AtomicInteger testCounter = new AtomicInteger(0);
		final String NOTIFY_NAME="myTestNotifyWorker";

		WorkerRunnable workerRunnable = () ->{
			//here you can grab things from the queue.
			//this code runs immediately after notify is raised

			//for test increase counter
			testCounter.incrementAndGet();

			//normally is the queue is not empty, we can continue
			//in this test we finish
			return false;
		};

		//setup worker processor, with 2 threads and wait time 60s if notification does not arrive (for example if there is some problem, restarting servers etc.)
		//normally put it in @PostConstruct section
		myWorkerProcessor.configure(2,NOTIFY_NAME,60*1000,"myWorker");
		myWorkerProcessor.start(workerRunnable);
		try {
			//worker runnable is started immediately when the worker processor started. Test counter should be 2 now.
			//send notification to two workers and increase Test counter.
			pgNotifyService.notify(NOTIFY_NAME);

			//wait little bit
			Thread.sleep(1000);

			//Test counter should be 4 now.
			Assert.isTrue(testCounter.get() == 4,"Two threads, should be 4.");

		} catch (InterruptedException e) {
			throw new RuntimeException("Interrupted");
		} finally {
			//clean, normally put it in @PreDestroy section
			myWorkerProcessor.stop();
		}
	}


}
