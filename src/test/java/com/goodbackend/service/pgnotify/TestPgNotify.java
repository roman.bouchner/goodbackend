package com.goodbackend.service.pgnotify;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.atomic.AtomicBoolean;

@SpringBootTest
public class TestPgNotify {

	private final PgNotifyService pgNotifyService;

	@Autowired
	public TestPgNotify(PgNotifyService pgNotifyService) {
		this.pgNotifyService = pgNotifyService;
	}

	@Test
	public void testNotify() {
		//prepare test variables
		AtomicBoolean testFlag = new AtomicBoolean(false);
		String EVENT_NAME = "myEvent";
		String PARAMETER = "testParameter";

		//setup listener
		PgCallback listener = (parameter -> {
			//received from notification
			//test parameter
			if(parameter.equals(PARAMETER)) {
				System.out.println("We have received a notification from our backends ! Parameter:" + parameter);
				//ok, set success flag
				testFlag.set(true);
			}
		});
		pgNotifyService.addServiceListener(EVENT_NAME,listener);

		try {
			//notify all backend connected to the same database
			pgNotifyService.notify(EVENT_NAME,PARAMETER);

			//wait for notification
			for(int i=0; i<5 ; i++) {
				//test flag
				if(testFlag.get()) {
					System.out.println("Received :)");
					return;
				}

				//wait little bit for the next try
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					throw new RuntimeException("Interrupted");
				}
			}
			throw new RuntimeException("No notification :(");

		} finally {
			//it is nice to clean up
			pgNotifyService.removeServiceListener(listener);
		}
	}


}
