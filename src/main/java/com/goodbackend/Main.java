package com.goodbackend;

import com.goodbackend.service.pgnotify.PgNotifyService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;


@SpringBootApplication
public class Main {

	final PgNotifyService pgNotifyService;

	public Main(PgNotifyService pgNotifyService) {
		this.pgNotifyService = pgNotifyService;
	}

	public static void main(String[] args) {
		SpringApplication.run(Main.class, args);
	}

	@EventListener(ApplicationReadyEvent.class)
	public void afterStart()  {
		System.out.println("STARTED");
	}
}
