package com.goodbackend.service.worker;

import java.io.IOException;

public interface WorkerRunnable {
	/**
	 * Worker implementation
	 * @return true if method should be called again immediately
	 * @throws InterruptedException
	 */
	boolean run() throws InterruptedException, IOException;
}
