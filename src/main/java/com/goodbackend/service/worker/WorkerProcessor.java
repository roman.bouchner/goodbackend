package com.goodbackend.service.worker;

import com.goodbackend.service.pgnotify.PgCallback;
import com.goodbackend.service.pgnotify.PgNotifyService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * This is helper prototype class for starting working threads.
 * The thread is waiting specified time or receives notification from pgNotifyService
 *
 * Class is marked as prototype so always a new instance is created.
 */
@Component
@Scope("prototype")
public class WorkerProcessor {

	private static final Logger log = LogManager.getLogger("WorkerProcessor");

	private static CopyOnWriteArrayList<WorkerProcessor> systemWorkers = new CopyOnWriteArrayList<>();

	private final PgNotifyService pgNotifyService;

	private List<Thread> workerThreads = new ArrayList<>();
	private List<OuterWorkerRunnable> workersRunnables = new ArrayList<>();

	private int numOfThreads;
	private String notifyName;
	private long waitTime;
	private String name;

	public WorkerProcessor(PgNotifyService pgNotifyService) {
		this.pgNotifyService = pgNotifyService;
	}

	/**
	 * Configures worker processor
	 *
	 * @param numOfThreads number of worker threads to create
	 * @param notifyName the name of notification to listen to. See PgNotifyService.
	 * @param waitTime wait time in milliseconds if no notification is received
	 * @param name name - is used in log
	 */
	public void configure(int numOfThreads, String notifyName, long waitTime, String name) {
		this.numOfThreads = numOfThreads;
		this.notifyName = notifyName;
		this.waitTime = waitTime;
		this.name = name;
	}

	/**
	 * Starts requested number of threads for worker processing.
	 * The stop method should be called when destroying application.
	 *
	 * @param clientRunnable - the thread content.
	 */
	public void start(WorkerRunnable clientRunnable) {
		systemWorkers.add(this);

		if(numOfThreads > 0) {
			//lets prepare workers
			for (int i = 0; i < numOfThreads; i++) {
				OuterWorkerRunnable outerWorkerRunnable = new OuterWorkerRunnable(clientRunnable);
				Thread worker = new Thread(outerWorkerRunnable);
				worker.setName(name + "-" + i);
				workerThreads.add(worker);
				workersRunnables.add(outerWorkerRunnable);
				worker.start();
				log.log(Level.INFO, "Created worker " + worker.getName());
			}

			if(notifyName != null) {
				//register listener for workers
				pgNotifyService.addServiceListener(notifyName, listener);
			}
		}
	}

	private PgCallback listener = (parameter) -> {
		//callback when notified about report request
		for (OuterWorkerRunnable outerWorkerRunnable : workersRunnables) {
			//notify workers about new work - worker will be awakened from wait state
			synchronized (outerWorkerRunnable) {
				outerWorkerRunnable.notify();
			}
		}
	};

	/**
	 * The stop method should be called when destroying application.
	 */
	public void stop() {
		//remove our listener
		pgNotifyService.removeServiceListener(listener);

		//first gently ask for stop
		for(OuterWorkerRunnable outerWorkerRunnable:workersRunnables) {
			//set stop flag
			outerWorkerRunnable.stopRunning();
			//notify about the change
			synchronized (outerWorkerRunnable) {
				outerWorkerRunnable.notify();
			}
		}

		//wait for finish
		for(Thread worker:workerThreads) {
			try {
				log.log(Level.INFO,"Waiting for " + worker.getName());
				worker.join(4*1000); //4s
				if(worker.isAlive()) {
					log.log(Level.WARN, "Worker " + worker.getName() + " does not respond");
				} else {
					log.log(Level.INFO, "Worker " + worker.getName() + " has finished");
				}
			} catch (InterruptedException e) {
				return;
			}
		}


		//send interrupt signal
		for(Thread worker:workerThreads) {
			if(worker.isAlive()){
				log.log(Level.INFO,"Sending interrupt signal to " + worker.getName());
				worker.interrupt();
			}
		}

		systemWorkers.remove(this);
	}

	private class OuterWorkerRunnable implements Runnable {
		private final WorkerRunnable runnable;
		private volatile boolean running;

		public OuterWorkerRunnable(WorkerRunnable runnable) {
			this.runnable = runnable;
			this.running = true;
		}

		@Override
		public void run() {
			while(running) {
				try {
					boolean again;
					//do the work
					do {
						log.log(Level.TRACE,"Start of " + Thread.currentThread().getName());
						again = runnable.run();

					} while(again && running);
					log.log(Level.TRACE,"End of " + Thread.currentThread().getName());

					//test if we should still be running
					if(!running) {
						break;
					}

					//wait until we are notified or wait time
					synchronized (this) {
						this.wait(waitTime);
					}

				} catch (InterruptedException e) {
					log.log(Level.INFO,"Worker " +Thread.currentThread().getName() + " interrupted");
					break;
				} catch (Exception e) {
					//do not finish this thread if error
					log.log(Level.ERROR,"Worker " +Thread.currentThread().getName() + " received unexpected exception.",e);

					//test if we should still be running
					if(!running) {
						break;
					}

					//wait until we are notified or wait time
					synchronized (this) {
						try {
							this.wait(waitTime);
						} catch (InterruptedException ex) {
							log.log(Level.INFO,"Worker " +Thread.currentThread().getName() + " interrupted");
							break;
						}
					}
				}
			}
		}

		public void stopRunning() {
			running = false;
		}
	}

	protected String getName() {
		return name;
	}

}
