package com.goodbackend.service.pgnotify;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Service for sending and receiving notifications through PostgreSQL database.
 * Typical usage:
 * We put work into the database table (queue) and we want to notify someone about it.
 */
@Service
public class PgNotifyService {

	private final JdbcTemplate jdbcTemplate;

	private CopyOnWriteArrayList<PgListener> listeners = new CopyOnWriteArrayList<>();
	private Thread notifyThread;

	protected static final String parameterSeparator = "/";

	public PgNotifyService(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@PostConstruct
	protected void start() {
		DataSource dataSource = jdbcTemplate.getDataSource();
		//start thread for listener
		notifyThread = new Thread(new PgListenerRunnable(dataSource,listeners));
		notifyThread.setName("PgListener");
		notifyThread.start();
	}

	@PreDestroy
	protected void stop() {
		notifyThread.interrupt();
	}

	/**
	 * Adds service receiver for requested notification event.
	 * @param eventName
	 * @param callBack
	 */
	public void addServiceListener(String eventName, PgCallback callBack) {
		testEventName(eventName);
		listeners.add(new PgListener(eventName,callBack));
	}

	/**
	 * Removes service receiver.
	 * @param pgCallback
	 */
	public void removeServiceListener(PgCallback pgCallback) {
		listeners.remove(pgCallback);
	}

	/**
	 * Sends notification event.
	 * Notify is part of the database transaction.
	 * @param eventName name of event
	 * @param eventParameter optional parameter, may be null
	 */
	public void notify(String eventName, String eventParameter) {
		testEventName(eventName);
		String event = eventName;
		if(eventParameter != null) {
			testEventName(eventParameter);
			event = event + parameterSeparator + eventParameter;
		}
		jdbcTemplate.execute("notify " + PgListenerRunnable.MESSAGE_NAME +", '"+event+"'");
	}

	/**
	 * Sends notification event.
	 * Notify is part of the database transaction.
	 * @param eventName name of event
	 */
	public void notify(String eventName) {
		notify(eventName,null);
	}

	private void testEventName(String name) {
		if(!name.matches("^[-\\w.]+")) {
			throw new RuntimeException("Invalid name " + name);
		}
	}
}
