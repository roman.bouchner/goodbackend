package com.goodbackend.service.pgnotify;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.postgresql.PGConnection;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Asks database for notifications.
 * Active asking has an advantage: if db connection fails, we now it immediately.
 * Should recover automatically from losing db connection
 */
public class PgListenerRunnable implements Runnable {
	private static final Logger log = LogManager.getLogger("PgListener");
	private Connection connection;
	private DataSource dataSource;

	private CopyOnWriteArrayList<PgListener> listeners;

	protected static final String MESSAGE_NAME = "BackendListener";

	protected PgListenerRunnable(DataSource dataSource, CopyOnWriteArrayList<PgListener> listeners) {
		//mark connection for listening
		this.listeners = listeners;
		this.dataSource = dataSource;
	}

	@Override
	public void run() {
		while(true) {
			try {
				log.log(Level.INFO, "PgListener has been started");
				//ask for new database connection
				this.connection = dataSource.getConnection();

				try {
					//mark connection for listening
					Statement stmt = connection.createStatement();
					try {
						stmt.execute("LISTEN " + MESSAGE_NAME);
					} finally {
						//always close
						stmt.close();
					}

					//start notification process loop
					processNotificationLoop(connection,listeners);
				} finally {
					//always close connection
					try {
						this.connection.close();
					} catch (SQLException e) {
						//does not matter now
						log.log(Level.ERROR, "Cannot close the connection to database", e);
					}
				}
			} catch (InterruptedException e) {
				log.log(Level.INFO, "PgListener has been interrupted.");
				//we should finish loop
				break;
			} catch (Exception e) {
				//maybe there is a database problem.
				log.log(Level.ERROR, "Cannot obtain database connection for PgListener.", e);
				//wait little bit
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e1) {
					log.log(Level.INFO, "PgListener has been interrupted.");
					//we should finish loop
					break;
				}
				//we want to run it again with different connection
				continue;
			}
		}
	}

	/**
	 * Process notification loop
	 *
	 */
	private static void processNotificationLoop(Connection connection, CopyOnWriteArrayList<PgListener> listeners) throws SQLException, InterruptedException {
		PGConnection pgConnection = connection.unwrap(PGConnection.class);
		while (true) {
			// issue a dummy query to contact the backend
			// and receive any pending notifications.
			// also we want to be sure, the database connection is still ok
			// btw - this is advantage of active querying ..
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT 1");
			rs.close();
			stmt.close();

			List<String> notifyList = new ArrayList<>();

			org.postgresql.PGNotification[] notifications = pgConnection.getNotifications();
			if (notifications != null) {
				for (int i = 0; i < notifications.length; i++) {
					notifyList.add(notifications[i].getParameter());
				}
			}

			for (String notify : notifyList) {
				String[] split = notify.split(PgNotifyService.parameterSeparator);
				String name = split[0];
				String parameter = split.length > 1?split[1]:null;
				for (PgListener listener : listeners) {
					if (name.equals(listener.getName())) {
						try {
							//do required code
							listener.getCallback().run(parameter);
						} catch (Throwable e) {
							//if any error, we do not want to stop this thread - just continue
							log.log(Level.ERROR, "Error during notify " + e.getMessage(), e);
						}
					}
				}
			}

			// wait a while before checking again for new
			// notifications
			Thread.sleep(500);
		}
	}
}
