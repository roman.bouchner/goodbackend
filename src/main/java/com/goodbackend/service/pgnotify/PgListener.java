package com.goodbackend.service.pgnotify;

public class PgListener {
	private final String name;
	private final PgCallback callback;

	public PgListener(String name, PgCallback callback) {
		this.name = name;
		this.callback = callback;
	}

	public String getName() {
		return name;
	}

	public PgCallback getCallback() {
		return callback;
	}
}
