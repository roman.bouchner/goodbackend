package com.goodbackend.service.pgnotify;

public interface PgCallback {
	/**
	 * Called when notification occurs
	 * @param parameter Optional parameter, can be null
	 */
	void run(String parameter);
}
