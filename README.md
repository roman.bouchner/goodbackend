# Good backend example files

## PostgreSQL notification
This is simple implementation for PostgreSQL notification system.

Please check _TestPgNotify_ class how to use it.

## Worker implementation
Workers can be used for receiving work from PostgreSQL queue.

Please check _TestWorker_ class how to use it.


If you like this, email me. I can extend this project with more useful examples.

Roman Bouchner
roman.bouchner@goodbackend.com


 